#!/usr/bin/python

"""
"""

__author__ = "Graham Horwood"
__license__ = "gpl"
__version__ = "1.1.4"
__email__ = "horwoodg@leidos.com"
__status__ = "development"


from docopt import docopt
from recaser import *


if __name__ == "__main__":

    args = {k.replace("-", ""): v
            for k, v in docopt(USAGE, version=__version__).items()}

    if args['fields']:
        fields = [int(a)-1 for a in args['fields'].split(',')]
        indata = fileinput(tsv)(args['FILE'])
    else:
        fields = [0]
        indata = fileinput(txt)(args['FILE'])

    if args["lms"] != "default.kenlm":
        models = args["lms"].split(",")
    else:
        models = [os.path.join(SCRIPTDIR, "models/default.kenlm")]

    if args["weights"]:
        weights = [float(w) for w in args["weights"].split(",")]
    else:
        weights = None

    if args['outfile'] == 'stdout':
        conn = sys.stdout
    else:
        conn = open(args['outfile'], 'ab')

    R = Recaser(cfgfile=args['config'],
                fields=fields,
                models=models,
                weights=weights,
                null=args['null'],
                pp_thr=args['cutoff'],
                strategy=args['strategy'],
                protfile=args['protected'])

    def recase(text):
        return R(text)

    try:
        cores = int(args['parallel'])
        if cores > 1:
            pool = Pool(processes=cores)
            out = pool.map(recase, indata)
            conn.write('\n'.join(out).encode('utf8')+'\n')
        else:
            for line in indata:
                conn.write(recase(line).encode('utf8')+'\n')
    except KeyboardInterrupt:
        pass
    except:
        raise

    sys.exit()
