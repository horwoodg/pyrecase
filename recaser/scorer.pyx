
import kenlm
from libc.math cimport log10


cdef float log10sum(float logx, float logy):
    """See: https://facwiki.cs.byu.edu/nlp/index.php/Log_Domain_Computations"""
    cdef float negdiff
    if logy > logx:
        logx, logy = logy, logx
    if logx == float("-inf"):
        return logx
    negdiff = logy - logx
    if negdiff < -20:
        return logx
    return logx + log10(1.0 + 10**negdiff)


cdef inline float interpolate(float wx, float logx, float wy, float logy):
    return log10sum(log10(wx) + logx, log10(wy) + logy)


cdef inline float _pp(float pr, int n):
    return 10**( -(1 / <float>n) * pr)


def pp(pr, n):
    return _pp(pr, n)


def lm_scorer(lmfiles, weights=None):
    cdef list lms, W
    lms = [kenlm.LanguageModel(lmfile)
           for lmfile in lmfiles]
    if len(lms) > 1 and not weights:
        W = [1.0/len(lms)] * len(lms)
    else:
        W = weights

    def score(candidates, context=False):
        cdef int n
        cdef float pr
        cdef list prs
        cdef list scores = []
        for cand in candidates:
            n = len(cand.split())
            pr = 0
            prs = []
            for lm in lms:
                prs.append(lm.score(cand, context))
            if len(lms) > 1:
                for i in range(len(W)-1):
                    pr += interpolate(W[i], prs[i],
                                      W[i+1], prs[i+1])
            else:
                pr = prs[0]
            scores.append(pr)
        return scores

    return score

