

cdef int MAX_ORDER = 3


cdef list combine(L):
    if len(L) == 1:
        return L[0]
    else:
        return [' '.join((w1, w2)) for w1 in L[0] for w2 in combine(L[1:])]


cdef list shortest_path(S):
    cdef list path = []
    cdef list tier, ties
    cdef tuple score, winner, s
    cdef int up, min_up
    cdef float max_pr
    cdef str c

    for tier in S:
        winner = None
        if len(tier) == 1:
            winner = tier[0]
        else:
            max_pr = max([s[0] for s in tier])
            ties = []
            for s in tier:
                if s[0] == max_pr:
                    ties.append(s)
            if len(ties) == 1:
                winner = ties[0]
            else:
                min_up = len(ties[0][1])
                for score in ties:
                    up = 0
                    for c in list(score[1]):
                        if c.isupper():
                            up += 1
                    if up < min_up:
                        winner = score
                        min_up = up
        path.append(winner)

    return path


cdef inline str most_common(tuple tpl):
    cdef list lst
    lst = [str(l) for l in tpl if l]
    return max(set(lst), key=lst.count)


cdef inline list pad(list A, int n, int m):
    return [None] * n + A + [None] * m

# NOT SURE WHY THIS DOESN'T WORK
#
#   cdef str collapse(list path):
#       cdef np.ndarray A
#       cdef list texts, W
#       cdef int n, i, j
#       if len(path) ==1:
#           return path[0][1]
#       else:
#           texts = []
#           n = len(path)
#           A = np.zeros([n+1,n+1], dtype='S128')
#           for i in range(n):
#               W = path[i][1].split()
#               for j, w in enumerate(W):
#                   try:
#                       A[i][i+j] = w
#                   except IndexError:
#                       pass
#           for col in A.T:
#               texts.append(most_common(tuple(col.tolist())))
#           return ' '.join(texts)


cdef str collapse(list path):
    cdef list B 
    cdef str text
    cdef int n

    B = [g[1].split() for g in path]
    n = len(path)
    text = ' '.join([most_common(c)
                    for c in zip(*[pad(B[i], i, n-i) for i in range(n)])[:-1]])
    return text


cdef class WordLattice:

    cdef public list data, lattice, scores

    def __init__(self, data):
        self.data = [d.encode('utf8') for d in data]
        self.build_lattice()

    def build_lattice(self):
        cdef int order
        cdef list sentences, L
        cdef tuple a
        cdef str s
        order = min((MAX_ORDER, len(self.data[0].split())))
        sentences = [s.split() for s in self.data]
        L = [list(set(a)) for a in zip(*sentences)]
        if order > 1:
            self.lattice = [combine(L[i:i+order]) for i in range(len(L)-1)]
        else:
            self.lattice = L

    def score(self, scorer, context=True):
        cdef list scores = []
        cdef list grams = []
        cdef list tier
        cdef int i
        self.scores = []
        for i, tier in enumerate(self.lattice):
            grams = [g.replace("\x00", "") for g in tier]
            if i > 0:
                context = False
            scores.append(scorer(grams, context))
        for i in range(len(scores)):
            self.scores.append(zip(scores[i], self.lattice[i]))

    def compose(self):
        cdef list path
        cdef str text
        cdef float pp
        path = shortest_path(self.scores)
        text = collapse(path)
        pp = sum([path[0][0]] + [g[0] for g in path[1:]])
        return (pp, text.decode('utf8'))


