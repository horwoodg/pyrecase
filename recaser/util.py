#!/usr/bin/python
# coding: utf-8

"""
"""

import sys, gzip
import __builtin__


__builtin__.VERBOSE = True



def fileinput(func):
    def readfile(fn=None):
        if fn:
            if fn.find(".gz") > -1:
                conn = gzip.open(fn, 'r')
            else:
                conn = open(fn, 'r')
        else:
            conn = sys.stdin
        for line in conn:
            newline = func(line.strip().decode('utf8'))
            yield newline
        conn.close()
    return readfile


def txt(text):
    return [text]


def tsv(text):
    return text.split("\t")
#!/usr/bin/python



class rgb:
    """For color coded ANSI output."""
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''


def seprint(msg, term="\n", col=rgb.OKBLUE):
    if VERBOSE:
        sys.stderr.write(col + msg + rgb.ENDC + term)
        sys.stderr.flush()


def console_counter(n=1000000, m=50000):
    if n >= 1000000:
        sfx = "M"
        div = lambda x: x//1000000
    elif n >= 1000:
        sfx = "K"
        div = lambda x: x//1000
    else:
        sfx = ""
        div = lambda x: x
    i = 1
    while i:
        if i % n == 0:
            seprint('(%d%s)' % (div(i), sfx), "")
        elif i % m == 0:
            seprint('.', "")
        i += 1
        yield
