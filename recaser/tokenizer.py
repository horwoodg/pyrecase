#!/usr/bin/python
# coding: utf-8

"""
"""

import sys, re


# SPACER = u" \ufffd "
SPACER = "\x00"

SFXS = ("t", "s", "ll", "d", "m", "ll", "re", "ve")
CONTRACTIONS = re.compile(u"(['\u2018\u2019])"+SPACER+" ("+"|".join(SFXS)+" )")

PUNCTUATION = u'-!"#$%&\'()*+,.:;<=>?@\[\]^`{|}~\u2018\u2019\u201a\u201b\u201c\u201d\u201e\u201f\u2012\u2013\u2014\u2015'

PUNCT = ((re.compile(u"(["+PUNCTUATION+"])([\w])", re.U),
          u"\\1" + SPACER + " \\2"),
         (re.compile(u"([\w])(["+PUNCTUATION+")])", re.U),
          u"\\1 " + SPACER + "\\2"))

BEGIN_S = re.compile(u"^( *[-\"'\u2018\u201a] *"+SPACER+"*)( \w)", re.U)


def tokenize(text):
    for ptn, rpl in PUNCT:
        text = ptn.sub(rpl, text)
    text = CONTRACTIONS.sub("'\\2", text)
    text = BEGIN_S.sub(u"\\1 <s> <s>\\2", text)
    return text


def detokenize(text):
    text = text.replace(" <s> <s>", "")
    text = text.replace(SPACER + " ", "").replace(" " + SPACER, "")
    return ' '.join(text.strip().split())


def main():

    for line in sys.stdin:
        line = line.strip().decode('utf8')
        line = tokenize(line).replace(SPACER, "")
        sys.stdout.write(line.encode('utf8')+"\n")
        sys.stdout.flush()


if __name__ == "__main__":

    sys.exit(main())
