#!/usr/bin/python

"""

The casing algorithm works as follows:

1) Generate candidate casings of the entire input sentence. In this example,
we will use two, lower case and title case.

['The Republican Party Lost the Election',
 'the republican party lost the election']

2) Generate a graph of trigrams combining the possible casings. There are at
most m^3 * n possible trigrams, for n the input sentence length and m candidate
casings. As the application supports a maximum of four candidate casing
strategies (source, upper, lower, title), time complexity is linear in sentence
length.

[['The republican Party', 'The republican party', 'The Republican Party',
  'The Republican party', 'the republican Party', 'the republican party',
  'the Republican Party', 'the Republican party'],
 ['republican Party lost', 'republican Party Lost', 'republican party lost',
  'republican party Lost', 'Republican Party lost',  'Republican Party Lost',
  'Republican party lost', 'Republican party Lost'],
 ['Party lost the', 'Party Lost the', 'party lost the', 'party Lost the'],
 ['lost the Election', 'lost the election', 'Lost the Election',
  'Lost the election'],
 ['the Election', 'the election']]

3) Score each resulting trigram on a language model. Return the most probable
candidate casing at each trigram tier.

[(-8.844243049621582, 'The Republican Party'),
 (-8.704912185668945, 'Republican Party lost'),
 (-7.7491302490234375, 'party lost the'),
 (-7.003618240356445, 'lost the election'),
 (-6.14015531539917, 'the election')]

4) Align the most probable trigrams word-wise. This is necessary in certain
cases, such as this one, where casings conflict at different tiers.

[('The',),
 ('Republican', 'Republican'),
 ('Party', 'Party', 'party'),  # |'Party'|=2, |'party'|=1
 ('lost', 'lost', 'lost'),
 ('the', 'the'),
 ('election',)]

5) Return the most commonly occurring casing for each word and the summed
probabilities of the best trigram path.

(-38.44205856323242, u'The Republican Party lost the election')


"""


__author__ = "Graham Horwood"
__license__ = "gpl"
__version__ = "1.1.4"
__email__ = "gruevyhat@gmail.com"
__status__ = "development"


import os
import sys
import json
from time import time
from multiprocessing import Pool
from docopt import docopt
from titlecase import titlecase
from word_lattice import WordLattice
from scorer import lm_scorer, pp
from util import fileinput, txt, tsv, seprint
from tokenizer import tokenize, detokenize


SCRIPTDIR = os.path.dirname(os.path.realpath(__file__))

USAGE = """
Recases input text according to a language model.

Usage: recaser.py [options] [ FILE | - ]

Options:
    -m, --lms lm1,lm2,...     A comma-delimited list of Kenlm LM paths.
                                [default: default.kenlm]
    -w, --weights w1,w1,...   A comma-delimited list of weights for LM
                                interpolation.
    -s, --strategy [fast|headline|all]
                              Seed casing strategy. [default: fast]
    -p, --protected FILE      Path to a protected word list (utf-8).
    -P, --parallel CORES      Number of parallel processes. [default: 1]
    -c, --config FILE         A configuration file in JSON format.
    -C, --cutoff n            Upper perplexity bound on output casings.
    -n, --null                Null LM context. Segments not scored with an
                                implied <s> tag.
    -f, --fields f1,f2,...    A comma-delimited list of TSV field numbers to
                                recase.
    -o, --outfile OUTFILE     Path to output result file. [default: stdout]
    -h, --help
    --version

"""


def word_lattice_caser(strategy=None, protfile=None):
    """Builds a graph of possible casings for an input sentence. Candidates will
    not be generated for words in the protected word file."""

    source = lambda x: x
    upper = lambda x: x.upper()
    lower = lambda x: x.lower()
    title = lambda x: titlecase(x)
    case_funcs = {'all': [source, upper, lower, title],
                  'fast': [lower, title],
                  'headline': [upper, title]}

    if strategy:
        casers = case_funcs[strategy]
    else:
        casers = case_funcs['fast']

    if protfile:
        protected = set(list(fileinput(lambda x: x)(protfile)) + ['<S>'])
    else:
        protected = set(['<S>'])

    def caser(text):
        src = text.split()
        cands = [f(text) for f in casers]
        cands = [' '.join((src[i] if w in protected else w
                           for i, w in enumerate(cand.split())))
                 for cand in cands]
        cands = list(set(cands))
        return WordLattice(cands)

    return caser


class Segment(object):
    """Basic container for all operations on an input sentence."""

    def __init__(self, text):
        self.raw = text
        self.n = len(text.split())

    def __repr__(self):
        return self.candidates

    def generate(self, gen):
        self.candidates = gen(self.raw)

    def score(self, scorer, context=False):
        self.candidates.score(scorer, context)
        self.scores = self.candidates.compose()

    def best(self, pp_thr=None):
        if pp_thr and pp(self.scores[0], self.n) > pp_thr:
            return self.raw
        return self.scores[1]


class Recaser(object):
    """Initializes models, generates candidates, scores them, filters them, and
    returns an optimum."""

    def __init__(self, cfgfile=None, fields=None,
                 null=None, models=None, weights=None,
                 pp_thr=None, strategy=None, protfile=None):
        if cfgfile:
            self._parse_cfg(cfgfile)
        else:
            self.cfg = {"null": null,
                        "models": models,
                        "weights": weights,
                        "pp_thr": pp_thr,
                        "strategy": strategy,
                        "protfile": protfile,
                        "fields": fields}
        t = time()
        seprint("Initializing...", "")
        self.fields = self.cfg['fields'] or [0]
        self.context = False if self.cfg['null'] else True
        self.scorer = lm_scorer(self.cfg["models"], self.cfg["weights"])
        self.cutoff = self.cfg['pp_thr']
        self.caser = word_lattice_caser(self.cfg['strategy'],
                                        self.cfg['protfile'])
        seprint("done (%ds)." % (time() - t))

    def __call__(self, line):
        out = []
        for s, seg in enumerate(line):
            if s in self.fields:
                seg = tokenize(seg)
                s = Segment(seg)
                s.generate(self.caser)
                s.score(self.scorer, self.context)
                best = s.best(self.cutoff)
                out += [detokenize(best)]
            else:
                out += [seg]
        return '\t'.join(out)

    def _parse_cfg(self, cfgfile):
        with open(cfgfile, 'r') as fi:
            self.cfg = {k: eval(v) if v in ("None", "True", "False") else v
                        for k, v in json.load(fi).iteritems()}


if __name__ == "__main__":

    args = {k.replace("-", ""): v
            for k, v in docopt(USAGE, version=__version__).iteritems()}

    if args['fields']:
        fields = [int(a)-1 for a in args['fields'].split(',')]
        indata = fileinput(tsv)(args['FILE'])
    else:
        fields = [0]
        indata = fileinput(txt)(args['FILE'])

    if args["lms"] != "default.kenlm":
        models = args["lms"].split(",")
    else:
        models = [os.path.join(SCRIPTDIR, "models/default.kenlm")]

    if args["weights"]:
        weights = [float(w) for w in args["weights"].split(",")]
    else:
        weights = None

    if args['outfile'] == 'stdout':
        conn = sys.stdout
    else:
        conn = open(args['outfile'], 'ab')

    R = Recaser(cfgfile=args['config'],
                fields=fields,
                models=models,
                weights=weights,
                null=args['null'],
                pp_thr=args['cutoff'],
                strategy=args['strategy'],
                protfile=args['protected'])

    def recase(text):
        return R(text)

    try:
        cores = int(args['parallel'])
        if cores > 1:
            pool = Pool(processes=cores)
            out = pool.map(recase, indata)
            conn.write('\n'.join(out).encode('utf8')+'\n')
        else:
            for line in indata:
                conn.write(recase(line).encode('utf8')+'\n')
    except KeyboardInterrupt:
        pass
    except:
        raise

    sys.exit()
