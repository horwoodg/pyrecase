PyRecase v.1.1.4
================

For a given input sentence, combinations of upper, lower, and title cased words are combined into a weighted trigram graph, the weight being the log probability of the trigram on a language model. The optimal casing is the graph path with the highest LM likelihood. Any language model generated with kenlm (https://kheafield.com/code/kenlm/) may be used, however, the current implementation of the recaser does not consider n-gram orders greater than three.

The application was developed for NLP practitioners seeking a simple and relatively fast recasing method not dependent upon a complex tool chain (as, i.e., the one found at http://www.statmt.org/moses/?n=Moses.SupportTools). 


Installation
------------

Standard Python package installation.

    > git clone https://bitbucket.org/horwoodg/pyrecase.git
    > cd pyrecase
    > python setup.py install


Usage
-----

The application is meant primarily for batch use. An initial load time in the tens of seconds should be expected the first time the recaser is run, as it loads the Kenlm model into memory. Subsequent to this initial overhead, the per-word recasing rate is quite fast, in the tens of milliseconds per word, depending on the input casing strategy.

* `all`: All possible combinations of source, upper, lower, and title case are considered for each word. (Default.)

* `fast`: Only lower and title cases are considered. Sufficient if the input data are not likely to contain many acronyms.

* `headline`: Only upper and title cases are considered.

Other command line options are as follows.

    Usage: recase [options] [ FILE | - ]

    Options:
        -s, --strategy [fast|headline|all|pos]
                                  Seed casing strategy. [default: fast]
        -m, --lms lm1,lm2,...     A comma-delimited list of Kenlm LM paths.
                                    [default: default.kenlm]
        -w, --weights w1,w1,...   A comma-delimited list of weights for LM
                                    interpolation.
        -p, --protected FILE      Path to a protected word list (utf-8).
        -P, --parallel CORES      Number of parallel processes. [default: 1]
        -c, --config FILE         A configuration file in JSON format.
        -C, --cutoff n            Upper perplexity bound on output casings.
        -n, --null                Null LM context. Segments not scored with an
                                    implied <s> tag.
        -f, --fields f1,f2,...    A comma-delimited list of TSV field numbers to
                                    recase.
        -o, --outfile OUTFILE     Path to output result file. [default: stdout]
        -h, --help
        --version

The `--null` option should be used when recasing lexicon entries and other segments which are not sentence-like. The `--protected` option exists primarily for lexicon recasing, as well, in the event that the input text might contain some correctly cased words. A list of 250K common names and acronyms is included in recaser/data/mobi.en (from http://www.infochimps.com/datasets/word-list-250000-hyphenated-capitalized-and-compound-english-wor).

The application currently supports simple linear interpolation of multiple language models, but does not do any optimization to determine an appropriate weighting scheme. Use at your own risk. 

Use of a configuration file is optional. All configuration options may be specified in a simple json file, containing, for example: 

    { "null": "False",
      "models": ["models/default.kenlm"],
      "weights": ["None"],
      "pp_thr": "None",
      "strategy": "all",
      "protfile": "None",
      "fields": [0] }
    
Note, finally, that the input is buffered when the application is parallelized.


Testing
-------

A simple test script `recaser-test` is installed to `/usr/local/bin` and may be used for bench marking the application on your system. 

    Usage: test.py <testdata> <srcdata> <kenlm> <strategy>


Dependencies
------------

PyRecase will work with versions of the following libraries no earlier than those below, installable with pip.

Cython>=0.20.2   
docopt>=0.6.1   
titlecase>=0.6.0   
python-Levenshtein>=0.10.1  

PyRecase is bundled with Kenlm, as it requires a minor modification to the Cython bindings of the Kenlm python extension at <https://github.com/kpu/kenlm>.  

The scripts have been tested on Python 2.7.x only.


Language Note
-------------

This version of PyRecase is only equipped to recase English text. The LM is trained on English GigaWord (https://catalog.ldc.upenn.edu/LDC2003T05) with an English-oriented tokenization strategy (all punctuation is tokenized except apostrophes adjacent to English contractions). A future version of PyRecase will add a command line option to specify other language-specific rules and models.