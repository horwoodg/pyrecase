#!/usr/bin/python

"""
For a given input sentence, combinations of upper, lower, and title cased words
are combined into a weighted trigram graph, the weight being the log probability
of the trigram on a language model. The optimal casing is the graph path with
the highest LM likelihood.
"""

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from glob import glob
import platform

INCDIRS = ['kenlm', 'kenlm/util', 'kenlm/lm',
           'kenlm/util/double-conversion']

FILES = []
for incdir in INCDIRS:
    FILES += [fn for fn in glob(incdir+'/*.cc')
              if not (fn.endswith('main.cc') or fn.endswith('test.cc'))]

LIBS = ['z', 'stdc++']
if platform.system() != 'Darwin':
    LIBS.append('rt')

ext_modules = [
    Extension(name='recaser.kenlm',
              sources=FILES + ['kenlm/python/kenlm.cpp'],
              language='C++',
              include_dirs=INCDIRS,
              libraries=LIBS,
              extra_compile_args=['-O3', '-DNDEBUG', '-DKENLM_MAX_ORDER=6']),
    Extension(name='recaser.scorer',
              sources=['recaser/scorer.pyx'],
              language='C'),
    Extension(name='recaser.word_lattice',
              sources=['recaser/word_lattice.pyx'],
              language='C'),
    ]

setup(
    name='pyrecase',
    version='1.1.4',
    author='Graham Horwood',
    author_email='gruevyhat@gmail.com',
    description='Recases input text according to a language model.',
    long_description=__doc__,
    license='http://www.gnu.org/licenses/gpl-2.0.html',
    platforms=['Unix'],
    url='https://bitbucket.org/horwoodg/pyrecase',
    download_url='https://bitbucket.org/horwoodg/pyrecase/get/1.1.4.tar.gz',
    keywords=['nlp', 'recaser'],
    packages=['recaser'],
    package_data={'recaser': ['models/default.kenlm',
                              'data/moby.en',
                              'data/test.src']},
    scripts=['recase', 'recaser-test'],
    cmdclass={'build_ext': build_ext},
    ext_modules=ext_modules
)
